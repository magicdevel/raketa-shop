select p1.`pm_id`, p1.`price`, `ts_day_start`, sum(`a_count`) * p1.price 
from purchases p1 
inner join (SELECT distinct `pm_id`, `price` FROM `purchases` order by pm_id limit 10) p2 on(p1.pm_id=p2.pm_id and p1.price=p2.price) 
group by p1.`pm_id`, p1.`price`, `ts_day_start`
order by p1.pm_id 
