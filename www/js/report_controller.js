ReportController = function(config) {
  // configurable {
  this.root = ''; // селектор корневого элемента
  this.reportUrl = ''; // адрес для загрузки таблицы отчёта
  this.rowsOnPage = 10;
  // }
  
  $.extend(this, config);
  
  this.requestParams = {}
  this.paginatorUpdating = false;
}

ReportController.prototype = {
  start: function() {
    this.initEvents();
    
    this._updateDateParams();
    this.initPaginator();
    this.load();
  },
  
  initEvents: function() {
    this.getDateFields().on('changeDate', $.proxy(this.onChangeDate, this));
  },
  
  initPaginator: function() {
    this.getPaginator().pagination({
        itemsOnPage: this.rowsOnPage,
        cssStyle: 'light-theme',
        onPageClick: $.proxy(this.onChangePage, this)
    });
  },
  
  load: function() {
    this.blockInterface();
    this.getReportBlock().load(this.reportUrl, this.requestParams, $.proxy(this.onLoadReport, this));
  },
  
  _updateDateParams: function() {
    this.requestParams.start = this.getStartField().val();
    this.requestParams.end = this.getEndField().val();
    
    if(!this.requestParams.start)
      delete this.requestParams.start;
    if(!this.requestParams.end)
      delete this.requestParams.end;
  },
  
  _updatePaginatorParams: function() {
    this.requestParams.offset = (this.getPaginator().pagination('getCurrentPage') - 1) * this.rowsOnPage;
  },
  
  _updatePaginator: function() {
    var $table = this.getReportTable();
    
    this.getPaginator().pagination('updateItems', $table.data('rows-count'));
    this.paginatorUpdating = true;
    this.getPaginator().pagination('selectPage', Math.round($table.data('offset') / this.rowsOnPage) + 1);
    this.paginatorUpdating = false;
  },
  
  blockInterface: function() {
    this.getRoot().block({ 
        message: 'Processing...'
    }); 
  },
  
  unblockInterface: function() {
    this.getRoot().unblock(); 
  },
  
  getPaginator: function() {
    return $('.paginator', this.getRoot());
  },
  
  getStartField: function() {
    return $(':input[name=start]', this.getControlsBlock());
  },
  
  getEndField: function() {
    return $(':input[name=end]', this.getControlsBlock());
  },
  
  getDateFields: function() {
    return $('.date', this.getControlsBlock());
  },
  
  getControlsBlock: function() {
    return $('.controls', this.getRoot());
  },
  
  getReportTable: function() {
    return $('table.report-table', this.getReportBlock());
  },
  
  getReportBlock: function() {
    return $('.report', this.getRoot());
  },
  
  getRoot: function() {
    return $(this.root);
  },
  
  onChangeDate: function() {
    this._updateDateParams();
    this.requestParams.offset = 0;
    
    this.load();
  },
  
  onChangePage: function() {
    if(!this.paginatorUpdating) {
      this._updatePaginatorParams();
    
      this.load();
    }
  },
  
  onLoadReport: function() {
    this.unblockInterface();
    this._updatePaginator();
  }
}