<?php
namespace Raketa\ShopBundle\Data;

use Doctrine\DBAL\Connection;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Генератор данных
 *
 * @author magicdevel
 */
class DataGenerator
{
  /**
   *
   * @var Connection
   */
  protected $connection;
  /**
   *
   * @var OutputInterface
   */
  protected $output;
    
  function __construct(Connection $connection, OutputInterface $output = null)
  {
    $this->connection = $connection;
    $this->output = $output;
  }
  
  /**
   * Генерация данных
   * 
   * @param \DateTime $start дата начала
   * @param \DateTime $end дата окончания
   * @param integer $productsCount количество товаров
   * @param integer $purchasesCount количество покупок
   */
  function generate(\DateTime $start, \DateTime $end, $productsCount, $purchasesCount)
  {
    $this->outLn('Generating products...');
    $products = $this->generateProducts($productsCount);
    
    $this->outLn('Generating purchases...');
    $this->generatePurchases($products, $start, $end, $purchasesCount);
  }
  
  function truncate()
  {
    $query = 'truncate purchases';
    $this->connection->executeQuery($query);
  }
  
  function createTable()
  {
    $query = 'create table if not exists purchases ('
      . 'ts_day_start int,'
      . 'pm_id int,'
      . 'price int,'
      . 'a_count int,'
      . 'index(pm_id, price, ts_day_start),'
      . 'index(ts_day_start, price)'
      . ')';
    
    $this->connection->executeQuery($query);
  }
  
  protected function generateProducts($productsCount)
  {
    $products = [];
    $signs = [1, -1];
    
    for($id = 1; $id <= $productsCount; $id ++)
    {
      // базовая цена товара
      $basePrice = mt_rand(1000, 15000);
      // коэффициент изменения цены товара
      $changeFactor = mt_rand(0, 100) / 100 * ($signs[mt_rand(0, 1)]);
      // частота изменения цены товара
      $changeFreq = mt_rand(0, 3);
      
      $products[] = [
        'id' => $id, 
        'basePrice' => $basePrice,
        'changeFactor' => $changeFactor,
        'changeFreq' => $changeFreq
      ];
    }
    
    return $products;
  }
  
  protected function generatePurchases($products, \DateTime $start, \DateTime $end, $purchasesCount)
  {
    // посчитаем количество дней между началом и концом
    $days = ($end->getTimestamp() - $start->getTimestamp()) / (60 * 60 * 24);
    $productsCount = count($products);
    
    $this->connection->executeQuery('alter table purchases disable keys');
    
    $query = $this->connection->prepare(
      'insert into purchases (ts_day_start, pm_id, price, a_count)'
      . ' values(?, ?, ?, ?)'
    );
    
    for($i = 0; $i < $purchasesCount; $i ++)
    {
      $productNum = mt_rand(0, $productsCount - 1);
      $productData = $products[$productNum];
      $day = mt_rand(0, $days);
      
      $price = $productData['basePrice'];
      
      if($productData['changeFreq'] > 0)
      {
        // если цена у товара может меняться, то рассчитываем изменение
        
        // определяем период изменения, в котором находится цена
        $daysInPeriod = round($days / $productData['changeFreq']);
        $period = round($day / $daysInPeriod);
        // определяем изменение цены в каждом периоде
        $factor = $productData['changeFactor'] / $productData['changeFreq'];
        // рассчитываем цену
        $price += $price * $factor * $period;
      }
      
      // количество купленного товара
      $count = mt_rand(1, 5);

      // определяем дату покупки
      $purchaseDate = strtotime("+$day days", $start->getTimestamp());
      $date = new \DateTime(date('Y-m-d', $purchaseDate).' 00:00:00', new \DateTimeZone('UTC'));

      // записываем данные в БД
      $query->bindValue(1, $date->getTimestamp(), \PDO::PARAM_INT);
      $query->bindValue(2, $productData['id'], \PDO::PARAM_INT);
      $query->bindValue(3, intval($price), \PDO::PARAM_INT);
      $query->bindValue(4, $count, \PDO::PARAM_INT);

      $query->execute();

      $this->out(strval(intval(($i + 1) / $purchasesCount * 100))."%\r");
    }
    
    $this->connection->executeQuery('alter table purchases enable keys');
    
    $this->outLn('');
  }

  protected function out($str)
  {
    if($this->output)
      $this->output->write($str);
  }
  
  protected function outLn($str)
  {
    if($this->output)
      $this->output->writeln($str);
  }
}
