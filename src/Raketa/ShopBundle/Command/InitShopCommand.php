<?php
namespace Raketa\ShopBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Doctrine\ORM\EntityManager;
use Raketa\ShopBundle\Data\DataGenerator;

/**
 * Description of InitShopCommand
 *
 * @author magicdevel
 */
class InitShopCommand extends ContainerAwareCommand
{
  protected function configure()
  {
    $this
      ->setName('raketa-shop:init')
      ->setDescription('Init raketa-shop.')
      ->addArgument('start-date', InputArgument::REQUIRED, 'Start date in format "YYYY-MM-DD"')
      ->addArgument('end-date', InputArgument::REQUIRED, 'End date in format "YYYY-MM-DD"')
      ->addArgument('products-count', InputArgument::REQUIRED)
      ->addArgument('purchases-count', InputArgument::REQUIRED)
    ;
  }
  
  protected function execute(InputInterface $input, OutputInterface $output)
  {
    $generator = new DataGenerator($this->getEntityManager()->getConnection(), $output);
    $generator->createTable();
    $generator->truncate();
    
    $startDate = new \DateTime($input->getArgument('start-date'));
    $endDate = new \DateTime($input->getArgument('end-date'));
    $productsCount = intval($input->getArgument('products-count'));
    $purchasesCount = intval($input->getArgument('purchases-count'));
    
    $generator->generate($startDate, $endDate, $productsCount, $purchasesCount);
  }
  
  /**
   * Returns entity manager
   * 
   * @return EntityManager
   */
  protected function getEntityManager()
  {
    return $this->getContainer()->get('doctrine')->getManager();
  }
}
