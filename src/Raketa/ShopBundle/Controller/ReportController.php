<?php
namespace Raketa\ShopBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Raketa\ShopBundle\Report\ReportCreator;
use Symfony\Component\HttpFoundation\Request;

/**
 * Description of ReportController
 *
 * @author magicdevel
 */
class ReportController extends Controller
{
  function indexAction()
  {
    return $this->render('RaketaShopBundle:Report:index.html.twig');
  }
  
  function loadAction(Request $request)
  {
    $start = $this->rusDateToDate($request->get('start', '01.05.2015'));
    $end = $this->rusDateToDate($request->get('end', '31.05.2015'));
    $offset = intval($request->get('offset', 0));
    
    if($start->getTimestamp() > $end->getTimestamp())
      list($start, $end) = [ $end, $start ];
    
    $report = new ReportCreator($this->getDoctrine()->getConnection());
    $reportData = $report->create($offset, 10, 'pm_id', 'asc', $start, $end);
    
    return $this->render('RaketaShopBundle:Report:report.html.twig', [
      'report' => $reportData
    ]);
  }
  
  /**
   * Преобразует русскую дату в обект DateTime
   * 
   * @param string $str
   * @return \DateTime 
   */
  protected function rusDateToDate($str)
  {
    if(!preg_match('#^(\d{2})\.(\d{2}).(\d{4})$#', $str, $matches))
      return new \DateTime('now');
    
    list(, $day, $month, $year) = $matches;
    
    return new \DateTime("$year-$month-$day 00:00:00", new \DateTimeZone('UTC'));
  }
}
