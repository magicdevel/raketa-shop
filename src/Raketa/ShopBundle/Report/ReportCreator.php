<?php
namespace Raketa\ShopBundle\Report;

use Doctrine\DBAL\Connection;

/**
 * Создатель отчёта
 *
 * @author magicdevel
 */
class ReportCreator
{
  protected $conn;
  protected $report;
    
  function __construct(Connection $conn)
  {
    $this->conn = $conn;
  }
  
  function create($offset, $limit, $orderBy, $orderDirect, \DateTime $start, \DateTime $end)
  {
    $data = $this->request($offset, $limit, $orderBy, $orderDirect, $start, $end);
    $this->report = [];
    
    foreach($data as $row)
      $this->addRequestRowToReport($row, $start, $end);
    
    return [
      'dates' => $this->getReportDates($start, $end),
      'orderBy' => $orderBy,
      'orderDirect' => $orderDirect,
      'offset' => $offset,
      'rowsCount' => $this->countRows($start, $end),
      'report' => $this->report
    ];
  }
  
  protected function addRequestRowToReport($row, \DateTime $start, \DateTime $end)
  {
    $id = $this->getRowId($row);
    if(!isset($this->report[$id]))
      $this->report[$id] = $this->createReportRow($row, $start, $end);
    
    $date = new \DateTime('@'.$row['ts_day_start']);
    $this->report[$id]['dates'][$this->dateToIndex($date)] = $row['sum'];
  }
  
  protected function createReportRow($row, \DateTime $start, \DateTime $end)
  {
    $dates = [];    
    $end = clone $end;
    $interval = \DateInterval::createFromDateString('1 day');
    $period = new \DatePeriod($start, $interval, $end->modify('+1 day'));

    foreach($period as $dt)
      $dates[$this->dateToIndex($dt)] = 0;
    
    return [
      'pm_id' => $row['pm_id'],
      'price' => $row['price'],
      'dates' => $dates
    ];
  }
  
  protected function getReportDates(\DateTime $start, \DateTime $end)
  {
    $dates = [];    
    $end = clone $end;
    $interval = \DateInterval::createFromDateString('1 day');
    $period = new \DatePeriod($start, $interval, $end->modify('+1 day'));

    foreach($period as $dt)
      $dates[] = $this->dateToIndex($dt);
    
    return $dates;
  }


  protected function getRowId($row)
  {
    return $row['pm_id'].'_'.$row['price'];
  }
  
  protected function dateToIndex(\DateTime $date)
  {
    return $date->format('d-m-Y');
  }

  protected function request($offset, $limit, $orderBy, $orderDirect, \DateTime $start, \DateTime $end)
  {
    return $this->conn->executeQuery($this->getQuery($offset, $limit, $orderBy, $orderDirect, $start, $end))->fetchAll();
  }
  
  protected function countRows(\DateTime $start, \DateTime $end)
  {
    $result = $this->conn->executeQuery(
      'SELECT count(*) FROM ('
      . ' SELECT distinct `pm_id`, `price` from `purchases`'
      . ' where ts_day_start between '.self::dateToSql($start).' and '.self::dateToSql($end)
      . ') p'
    );
    
    $row = $result->fetch();
    return reset($row);
  }

  protected function getSubQuery($offset, $limit, $orderBy, $orderDirect, \DateTime $start, \DateTime $end)
  {
    return 'SELECT distinct `pm_id`, `price`'
      . ' FROM `purchases`'
      . ' where ts_day_start between '.self::dateToSql($start).' and '.self::dateToSql($end)
      . ' order by '.self::getOrderField($orderBy, $orderDirect).' limit '.  intval($offset).', '.intval($limit);
  }
  
  protected function getQuery($offset, $limit, $orderBy, $orderDirect, \DateTime $start, \DateTime $end)
  {
    return 'select p1.`pm_id`, p1.`price`, `ts_day_start`, sum(`a_count`) * p1.price `sum`'
      . ' from purchases p1'
      . ' inner join ('.$this->getSubQuery($offset, $limit, $orderBy, $orderDirect, $start, $end).') p2 on(p1.pm_id = p2.pm_id and p1.price = p2.price)'
      . ' where ts_day_start between '.self::dateToSql($start).' and '.self::dateToSql($end)
      . ' group by p1.`pm_id`, p1.`price`, `ts_day_start`'
      . ' order by '.self::getOrderField($orderBy, $orderDirect, 'p1.');
  }

  static function dateToSql(\DateTime $date)
  {
    $day = new \DateTime($date->format('Y-m-d').' 00:00:00', new \DateTimeZone('UTC'));
    return $day->getTimestamp();
  }
  
  static function getOrderField($orderBy, $orderDirect, $prefix = '')
  {
    $orders = ['pm_id', 'price'];
    
    $fields = [];
    
    $field = $orderBy;
    
    if(!in_array($field, $orders))
      $field = 'pm_id';
    
    if($orderDirect == 'desc')
      $field .= ' desc';
    
    $field = $prefix.$field;
    
    $fields[] = $field;
    
    if($orderBy == 'pm_id')
      $fields[] = $prefix.'price';
    
    return implode(', ', $fields);
  }
}
